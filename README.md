# Autor: Oleksandr Lysenko

The project is an example LCA implementation, current implementation based on b-directional trees. 

Here is some basic assumptions: 

- Root node is always present(Claire)
- Each node can have multiple parent and chield nodes
- Assuming that node can be identified by employee name
- No validation needed.(BTW some basic validation has been added)
- Time complexity in optimistic cases for search with the implementation O(n) ~ l, where l is depth of tree, O(n) ~ n in worths case.

## Features

- This solution can be extended for any problem from the class of problem just by implementing Element interface(Company structure, etc).
- Used TTD approach, goal here was cover hierarchy package.

## Tech

Used golang 1.14 without any 3 party dependency:

## Run/Test app

Simple way to check application funcitonality is by running UT

```
go test $(go list ./... | grep -v /vendor/) -v
```

Or use console app interface

```
go run main.go
```

## Instructions

    The application supports 2 operators '->' and '<->'
	
	To add child node, please use '->'(A -> B), if child node does not exist then it will be created automatically, parent node needs to be exsting.
	
	To find common Manager, please use '<->'(A <-> B)
	
	To build hierarchy like:
              Claire
             B1    B2
           C1  C2  C3  C4
                D1
                
	Use the next sequence of commands(Assuming that root node is always present):
		
	Claire -> B1
	Claire -> B2
	B1 -> C1
	B1 -> C2
	C2 -> D1
	B2 -> C3
	B2 -> C4
	
	To get manager let's say got C1 and C3 enter C1 <-> C3
```
