package repository

import (
	"errors"
	"test.com/app/models"
	"test.com/hierarchy"
)

var rootNode *hierarchy.Node
var rootElement hierarchy.Element

// Assuming that CEO is always present
func init() {
	rootElement = &models.Employee{Name: "Claire"}
	rootNode = hierarchy.NewNode(rootElement)
}

//Add(employee models.Employee) *models.Employee
//Search(employee models.Employee) *models.Employee
//AddReporter(manager models.Employee, reporter models.Employee) *models.Employee
//AddManager(reporter models.Employee, manager models.Employee) *models.Employee

type EmployeeRepositoryImplementation struct {
}

func (i *EmployeeRepositoryImplementation) search(employee hierarchy.Element) (*hierarchy.Node, error) {
	result := rootNode.Search(employee)
	if result == nil{
		return nil, errors.New(employee.GetIdentifier() + " Does not exist, please, adding before use")
	}

	return result, nil
}

func (i *EmployeeRepositoryImplementation) AddReporter(manager hierarchy.Element, reporter hierarchy.Element) (hierarchy.Element, error) {
	parentNode, error := i.search(manager)
	if error != nil {
		return nil, error
	}

	childNode, _ := i.search(reporter)
	if childNode == nil {
		node := hierarchy.NewNode(reporter)
		parentNode.AddChild(node)
		node.AddParent(parentNode)
	}

	return reporter, nil
}

func (i *EmployeeRepositoryImplementation) NearestCommonManager(manager hierarchy.Element, reporter hierarchy.Element) (hierarchy.Element, error) {
	a, error := i.search(manager)
	if error != nil {
		return nil, error
	}

	b, error := i.search(reporter)

	if error != nil {
		return nil, error
	}

	return a.FindNearestCommonAncestor(b).HierarchyElement, nil
}
