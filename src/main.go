package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"test.com/app"
)

func main() {
	fmt.Print( app.Hello())

	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print("-> ")
		command, _ := reader.ReadString('\n')
		command = strings.Replace(command, "\n", "", -1)

		message := app.Command(command)

		fmt.Println(message)
	}
}
