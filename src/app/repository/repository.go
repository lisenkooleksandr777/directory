package repository

import (
	"test.com/hierarchy"
)

// EmployeeRepository - lets assume we can only add new employees and search in a collection of Employees
type EmployeeRepository interface {
	AddReporter(manager hierarchy.Element, reporter hierarchy.Element) (hierarchy.Element, error)

	NearestCommonManager(manager hierarchy.Element, reporter hierarchy.Element) (hierarchy.Element, error)
}
