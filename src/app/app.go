package app

import (
	"strings"
	"test.com/app/models"
	"test.com/app/repository"
	repoImplementation "test.com/repository"
)

var repo repository.EmployeeRepository

func init() {
	repo = &repoImplementation.EmployeeRepositoryImplementation {}
}

func Hello() string {
	return `
		Hi, I support only 2 operators '->' and '<->'
		To add child node, please use '->'(A -> B), if child node does not exist then it will be created automatically, 
		parent node needs to be exsting.
		To find common Manager, please use '<->'(A <-> B)
		To build hierarchy like:
          Claire
         B1    B2
      C1  C2  C3  C4
           D1
		
		Use the next sequence of commands(Assuming that root node is always present):
		
		Claire -> B1
		Claire -> B2
		B1 -> C1
		B1 -> C2
		C2 -> D1
		B2 -> C3
		B2 -> C4
	`
}

func Command(command string) string  {
	words := strings.Fields(command)

	if len(words)!= 3 {
		return "Expected 2 arguments and operator like: A -> B"
	}

	operator := words[1]

	a:= models.Employee{Name: words[0]}
	b:= models.Employee{Name: words[2]}

	if operator == "->"{
		_, error := repo.AddReporter(&a, &b)
		if error != nil{
			return error.Error()
		}

		return "Relation " + command + " has been added"
	} else if operator =="<->" {
		result, error := repo.NearestCommonManager(&a, &b)
		if error != nil{
			return error.Error()
		}

		return "Nearest common manager is " + result.GetIdentifier()
	}

	return "It might be an incorrect input..."
}

