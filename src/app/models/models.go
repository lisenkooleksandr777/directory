package models

//Employee - lets assume that an employee can be identified by name
type Employee struct {
	Name string
}

func (e *Employee) GetIdentifier() string {
	return e.Name
}
