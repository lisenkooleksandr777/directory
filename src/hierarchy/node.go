package hierarchy

/* Such Approach  can be used to represent any type of Hierarchical structure: employees, Company structure etc.
So, rather then just  copy/paste or use  code generation Approach in order to make it Extendable lets use interface Instead. */
type Node struct {
	Parent           map[string]*Node
	Children         map[string]*Node
	HierarchyElement Element
}

func NewNode(element Element) *Node {
	return &Node{
		Parent:           make(map[string]*Node),
		Children:         make(map[string]*Node),
		HierarchyElement: element,
	}
}

// Search - return element from hierarchy if it exists
func (n *Node) Search(element Element) *Node {
	if n.HierarchyElement.GetIdentifier() == element.GetIdentifier() {
		return n
	}

	searchResult := n.Children[element.GetIdentifier()]
	if searchResult != nil{
		return searchResult
	}

	for _, item := range n.Children {
		searchResult = item.Search(element)
		if searchResult != nil {
			return searchResult
		}
	}

	return nil
}

//AddParent - add a parent to the node if a parent has not been added yet
func (n *Node) AddParent(parent *Node) *Node {
	existingItem:= n.Parent[parent.HierarchyElement.GetIdentifier()]

	if existingItem == nil {
		n.Parent[parent.HierarchyElement.GetIdentifier()] = parent
	}

	return n
}

//AddChild - add a child to the node if parent has not been added yet
func (n *Node) AddChild(child *Node) *Node {
	existingItem:= n.Children[child.HierarchyElement.GetIdentifier()]
	if existingItem == nil {
		n.Children[child.HierarchyElement.GetIdentifier()] = child
	}

	return n
}

//FindNearestCommonAncestor - searching for the nearest ancestor, in general there is at list single ancestor present - root node
func (n *Node) FindNearestCommonAncestor(otherElement *Node) *Node {

	if n.HierarchyElement.GetIdentifier() == otherElement.HierarchyElement.GetIdentifier() {
		return n
	}

	searchResult := n.Search(otherElement.HierarchyElement)
	if searchResult == nil {
		for _, item := range n.Parent {
			searchResult = item.FindNearestCommonAncestor(otherElement)
			if searchResult != nil {
				return searchResult
			}
		}
	}

	return n
}
