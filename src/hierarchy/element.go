package hierarchy

type Element interface {
	GetIdentifier() string
}