package hierarchy

import "testing"

/* Lets use a tree below for UTs
            A
         B1    B2
      C1  C2  C3  C4
           D1
In this case for C1 and D1 nearest ancestor is B1, but A is as well common ancestor, lets assume that distance between all of nodes is 1
There are some edge cases like Ancestor of C1 and B1, in this case B1 is LCA

Our test cases are next:
	B1 & B1 = B1
	B1 & B2 = A
	C1 & D1 = B1
	D1 & C4 = A
	C3 & C4 = B2
	C2 & D1 = C2
*/

type MockEmployee struct {
	Id string
}

func (e *MockEmployee) GetIdentifier() string {
	return e.Id
}

var rootElement Element
var rootNode *Node
func init()  {
	rootElement = &MockEmployee{Id: "A"}
	rootNode = NewNode(rootElement)


	b1 := &MockEmployee{Id: "B1"}
	b1Node := NewNode(b1)
	rootNode.AddChild(b1Node)
	b1Node.AddParent(rootNode)

	b2 := &MockEmployee{Id: "B2"}
	b2Node := NewNode(b2)
	rootNode.AddChild(b2Node)
	b2Node.AddParent(rootNode)

	c1 := &MockEmployee{Id: "C1"}
	c1Node := NewNode(c1)
	b1Node.AddChild(c1Node)
	c1Node.AddParent(b1Node)

	c2 := &MockEmployee{Id: "C2"}
	c2Node := NewNode(c2)
	b1Node.AddChild(c2Node)
	c2Node.AddParent(b1Node)

	c3 := &MockEmployee{Id: "C3"}
	c3Node := NewNode(c3)
	b2Node.AddChild(c3Node)
	c3Node.AddParent(b2Node)

	c4 := &MockEmployee{Id: "C4"}
	c4Node := NewNode(c4)
	b2Node.AddChild(c4Node)
	c4Node.AddParent(b2Node)

	d1 := &MockEmployee{Id: "D1"}
	d1Node := NewNode(d1)
	c2Node.AddChild(d1Node)
	d1Node.AddParent(c2Node)
}

// TestSearch1 - just testing if search works
func TestSearchExistingItem(t *testing.T)  {
	d1 := &MockEmployee{Id: "D1"}

	result := rootNode.Search(d1)

	if result == nil {
		t.Errorf("Search(%s) has failed, expected non nil result", d1.GetIdentifier())
	} else{
		t.Logf("Search(%s) success, found %s", d1.GetIdentifier() ,result.HierarchyElement.GetIdentifier())
	}
}

// TestSearch2 - Searching for non-existing item
func TestSearchNonExistingItem(t *testing.T)  {
	k1 := &MockEmployee{Id: "K1"}

	result := rootNode.Search(k1)

	if result == nil {
		t.Logf("Search(%s) success, no items found, since the element does not exist the tree", k1.GetIdentifier())
	} else{
		t.Errorf("Search(%s) has failed, following element does not exist", k1.GetIdentifier())
	}
}

func TestNearestAncestorB1toB1(t *testing.T)  {
	b1 := &MockEmployee{Id: "B1"}

	b1Node := rootNode.Search(b1)

	result := b1Node.FindNearestCommonAncestor(b1Node)

	expectedResult := "B1"
	if result.HierarchyElement.GetIdentifier() == expectedResult {
		t.Logf("FindNearestCommonAncestor(%s) success, expected %s - got %s", b1.GetIdentifier(),expectedResult, result.HierarchyElement.GetIdentifier())
	} else{
		t.Errorf("FindNearestCommonAncestor(%s) has failed, expected %s but got %s", b1.GetIdentifier(), expectedResult, result.HierarchyElement.GetIdentifier())
	}
}

func TestNearestAncestorB1toB2(t *testing.T)  {
	b1 := &MockEmployee{Id: "B1"}
	b2 := &MockEmployee{Id: "B2"}

	b1Node := rootNode.Search(b1)
	b2Node := rootNode.Search(b2)

	result := b1Node.FindNearestCommonAncestor(b2Node)

	expectedResult := "A"
	if result.HierarchyElement.GetIdentifier() == expectedResult {
		t.Logf("FindNearestCommonAncestor(%s) success, expected %s - got %s", b1.GetIdentifier(),expectedResult, result.HierarchyElement.GetIdentifier())
	} else{
		t.Errorf("FindNearestCommonAncestor(%s) has failed, expected %s but got %s", b1.GetIdentifier(), expectedResult, result.HierarchyElement.GetIdentifier())
	}
}

func TestNearestAncestorC1toD1(t *testing.T)  {
	c1 := &MockEmployee{Id: "C1"}
	d1 := &MockEmployee{Id: "D1"}

	c1Node := rootNode.Search(c1)
	d2Node := rootNode.Search(d1)

	result := c1Node.FindNearestCommonAncestor(d2Node)

	expectedResult := "B1"
	if result.HierarchyElement.GetIdentifier() == expectedResult {
		t.Logf("FindNearestCommonAncestor(%s) success, expected %s - got %s", d1.GetIdentifier(),expectedResult, result.HierarchyElement.GetIdentifier())
	} else{
		t.Errorf("FindNearestCommonAncestor(%s) has failed, expected %s but got %s", d1.GetIdentifier(), expectedResult, result.HierarchyElement.GetIdentifier())
	}
}

func TestNearestAncestorC4toD1(t *testing.T)  {
	c4 := &MockEmployee{Id: "C4"}
	d1 := &MockEmployee{Id: "D1"}

	c1Node := rootNode.Search(c4)
	d2Node := rootNode.Search(d1)

	result := c1Node.FindNearestCommonAncestor(d2Node)

	expectedResult := "A"
	if result.HierarchyElement.GetIdentifier() == expectedResult {
		t.Logf("FindNearestCommonAncestor(%s) success, expected %s - got %s", d1.GetIdentifier(),expectedResult, result.HierarchyElement.GetIdentifier())
	} else{
		t.Errorf("FindNearestCommonAncestor(%s) has failed, expected %s but got %s", d1.GetIdentifier(), expectedResult, result.HierarchyElement.GetIdentifier())
	}
}

func TestNearestAncestorC3toC4(t *testing.T)  {
	c3 := &MockEmployee{Id: "C3"}
	c4 := &MockEmployee{Id: "C4"}

	c3Node := rootNode.Search(c3)
	c4Node := rootNode.Search(c4)

	result := c3Node.FindNearestCommonAncestor(c4Node)

	expectedResult := "B2"
	if result.HierarchyElement.GetIdentifier() == expectedResult {
		t.Logf("FindNearestCommonAncestor(%s) success, expected %s - got %s", c4.GetIdentifier(),expectedResult, result.HierarchyElement.GetIdentifier())
	} else{
		t.Errorf("FindNearestCommonAncestor(%s) has failed, expected %s but got %s", c4.GetIdentifier(), expectedResult, result.HierarchyElement.GetIdentifier())
	}
}

func TestNearestAncestorC2toD1(t *testing.T)  {
	c2 := &MockEmployee{Id: "C2"}
	d1 := &MockEmployee{Id: "D1"}

	c1Node := rootNode.Search(c2)
	d2Node := rootNode.Search(d1)

	result := c1Node.FindNearestCommonAncestor(d2Node)

	expectedResult := "C2"
	if result.HierarchyElement.GetIdentifier() == expectedResult {
		t.Logf("FindNearestCommonAncestor(%s) success, expected %s - got %s", d1.GetIdentifier(),expectedResult, result.HierarchyElement.GetIdentifier())
	} else{
		t.Errorf("FindNearestCommonAncestor(%s) has failed, expected %s but got %s", d1.GetIdentifier(), expectedResult, result.HierarchyElement.GetIdentifier())
	}
}